export class GenericResponseModel {

    listaRespuesta: Array<any>;
    objetoRespuesta: any;
    respuesta: string;
    variable: string;

    constructor() {
        this.listaRespuesta = new Array();
        this.objetoRespuesta = null;
        this.respuesta = '';
        this.variable = '';
    }
}
