import { MasterModel } from './master-model';

export class VehicleModel {
    id: number;
    capacidad: number;
    nombre: string;
    idTipoVehiculo: MasterModel;

    constructor() {
        this.id = null;
        this.capacidad = null;
        this.nombre = '';
        this.idTipoVehiculo = new MasterModel();
    }
}
