export class FlyModel {
    idAvion: number;
    distanUser: number;
    altUser: number;
    personParacaida: number
    action: string;
    fechaRegistro: Date;

    constructor() {
        this.idAvion = null;
        this.distanUser = null;
        this.altUser = null;
        this.personParacaida = null;
        this.action = null;
        this.fechaRegistro = null;
    }

}
