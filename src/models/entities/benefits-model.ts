import { RatingModel } from './rating-model';
export class BenefitsModel {
    id: number;
    nombre: string;
    descripcion: string;
    idTarifa: RatingModel;

    constructor() {
        this.id = null;
        this.nombre = '';
        this.descripcion = '';
        this.idTarifa = new RatingModel();
    }
}
