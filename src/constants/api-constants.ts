export const rating =  {
    apiSave: 'tarifa/api/guardarTarifa',
    apiGet: 'tarifa/api/obtenerTarifas',
    apiDelete: 'tarifa/api/eliminarTarifa'
};

export const benefits =  {
    apiSave: 'bene/api/guardarBeneficio',
    apiGet: 'bene/api/listarBeneficio',
    apiDelete: 'bene/api/eliminarBeneficio'
};

export const vehicle =  {
    apiSave: 'vehiculo/api/guardar-o-actualizar',
    apiGet: 'vehiculo/api/listarVehiculo',
    apiDelete: 'vehiculo/api/borrar'
};

export const maestroValor =  {
    apiGet: 'mv/api/obtener-por-tipo',
};
export const user =  {
    apiSave: 'usr/api/guardarOActualizarUsuario',
    apiGet: 'usr/api/obtenerUsuarios',
    apiGetFilter: 'usr/api/obtenerUsuariosFiltro',
    apiDelete: 'usr/api/eliminarUsuario'
};

export const benefit = {
    apiSave: 'bene/api/guardarBeneficio',
    apiGet: 'bene/api/listarBeneficio',
    apiDelete: 'bene/api/eliminarBeneficio'
}

export const valueMaster = {
    apiGetByType: 'mv/api/obtener-por-tipo'
}

export const avion = {
    apiSave: 'avion/api/guardar',
    apiGet: 'avion/api//obtenerAvion'
}
