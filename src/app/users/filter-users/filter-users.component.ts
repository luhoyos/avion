import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FilterUsersModel } from './../../../models/searching/filter-users-model';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-filter-users',
  templateUrl: './filter-users.component.html',
  styleUrls: ['./filter-users.component.scss']
})
export class FilterUsersComponent implements OnInit {

  @Output() eventEmitter: EventEmitter<any> = new EventEmitter();

  filterUserForm: FormGroup;
  filterUserInfo: FilterUsersModel = new FilterUsersModel();

  constructor(
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.filterUserForm = this.formBuilder.group({
      userNameValidator: ['', [Validators.required]],
      userDocumentValidator: ['', [Validators.required]]
    });
  }

  onSubmit() {
    this.eventEmitter.emit(this.filterUserInfo);
  }

  get validatorForm() { return this.filterUserForm.controls; }

}
