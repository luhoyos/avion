import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { environment } from './../../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class BackendServiceService {

  constructor(private http: HttpClient) { }

  public getService(url, paramsIn: any) {
    return this.http.get(environment.domainUrl + url, {
      params: paramsIn
    }).toPromise();
  }

  public postService(url, paramsIn: any, body: any) {
    return this.http.post(environment.domainUrl + url, body, {
      params: paramsIn
    }).toPromise();
  }

  public deleteService(url, paramsIn: any) {
    return this.http.delete(environment.domainUrl + url, {
      params: paramsIn
    }).toPromise();
  }

  public putService(url, paramsIn: any, body: any) {
    return this.http.put(environment.domainUrl + url, body, {
      params: paramsIn
    }).toPromise();
  }
}
