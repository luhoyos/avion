import { rating } from './../../../../constants/api-constants';
import { GenericResponseModel } from './../../../../models/response/generic-response-model';
import { ComunicationModel } from './../../../../models/utils/comunication-model';
import { RatingModel } from './../../../../models/entities/rating-model';
import { FormRatingsComponent } from './../form-ratings/form-ratings.component';
import { BackendServiceService } from './../../../shared/backend-service/backend-service.service';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-list-ratings',
  templateUrl: './list-ratings.component.html',
  styleUrls: ['./list-ratings.component.scss']
})
export class ListRatingsComponent implements OnInit {

  ratings: Array<RatingModel> = new Array();

  columns = [
    { prop: 'id' },
    { prop: 'valor' },
    { prop: 'options' }
  ];

  constructor(
    private backendService: BackendServiceService,
    private ngModal: NgbModal
  ) {
  }

  ngOnInit() {
    this.getRatings();
  }

  getRatings() {
    this.backendService.getService(rating.apiGet, {
      'idTarifa': 1
    }).then(
      (getRaitingResult: GenericResponseModel) => {
        this.ratings = [...getRaitingResult.listaRespuesta];
      }
    ).catch(
      (getRaitingError) => {
        console.error(getRaitingError);
      }
    )
  }

  viewRating(ratingToView) {
    const modalOpened = this.ngModal.open(FormRatingsComponent, {
      backdrop: 'static',
      size: 'lg'
    });
    modalOpened.componentInstance.ratingModelInfo = { ...ratingToView };
    const comunicationModelInfo: ComunicationModel = new ComunicationModel();
    comunicationModelInfo.mode = 'view';
    modalOpened.componentInstance.comunicationModelInfo = comunicationModelInfo;
  }

  editRating(ratingToView) {
    const modalOpened = this.ngModal.open(FormRatingsComponent, {
      backdrop: 'static',
      size: 'lg'
    });
    modalOpened.componentInstance.ratingModelInfo = { ...ratingToView };
    const comunicationModelInfo: ComunicationModel = new ComunicationModel();
    comunicationModelInfo.mode = 'edit';
    modalOpened.componentInstance.comunicationModelInfo = comunicationModelInfo;
    modalOpened.componentInstance.eventEmitter.subscribe(
      (response) => {
        if (response === 'Cool') {
          this.getRatings();
          modalOpened.dismiss();
        }
      }
    )
  }

  createRating() {
    const modalOpened = this.ngModal.open(FormRatingsComponent, {
      backdrop: 'static',
      size: 'lg'
    });
    const comunicationModelInfo: ComunicationModel = new ComunicationModel();
    comunicationModelInfo.mode = 'create';
    modalOpened.componentInstance.comunicationModelInfo = comunicationModelInfo;
    modalOpened.componentInstance.eventEmitter.subscribe(
      (response) => {
        if (response === 'Cool') {
          this.getRatings();
          modalOpened.dismiss();
        }
      }
    )
  }

  deleteRating(idToDelete) {
    this.backendService.deleteService(rating.apiDelete, {
      idTarifaABorrar: idToDelete
    }).then(
      (deleteRaitingResult: GenericResponseModel) => {
        if (deleteRaitingResult.respuesta === 'EXITO') {
          Swal.fire({
            title: 'Super tu servicio',
            text: '¡Que efectividad!',
            icon: 'success',
            confirmButtonText: 'Cool'
          }).then(
            () => {
              this.getRatings();
            }
          )
        }
      }
    ).catch(
      (deleteRaitingError) => {
        console.error(deleteRaitingError);
      }
    )
  }

}
