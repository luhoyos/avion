import { vehicle, maestroValor } from './../../../../constants/api-constants';
import { VehicleModel } from './../../../../models/entities/vehicle-model';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ComunicationModel } from 'models/utils/comunication-model';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BackendServiceService } from 'app/shared/backend-service/backend-service.service';
import { GenericResponseModel } from 'models/response/generic-response-model';
import Swal from 'sweetalert2';
import { MasterModel } from 'models/entities/master-model';

@Component({
  selector: 'app-form-vehicle',
  templateUrl: './form-vehicle.component.html',
  styleUrls: ['./form-vehicle.component.scss']
})
export class FormVehicleComponent implements OnInit {

  vehicleForm: FormGroup;
  tipoVehiculo: Array<MasterModel> = new Array();

  @Input() vehicleModelInfo: VehicleModel = new VehicleModel();
  @Input() comunicationModelInfo: ComunicationModel = new ComunicationModel();
  @Output() eventEmitter: EventEmitter<any> =  new EventEmitter();

  constructor(
    private formBuilder: FormBuilder,
    private ngbActiveModal: NgbActiveModal,
    private backendService: BackendServiceService
  ) { }

  ngOnInit() {
    this.buildForm();
    this.getVehicles();
  }

  buildForm() {
    if (!(this.comunicationModelInfo.mode === 'view')) {
      this.vehicleForm = this.formBuilder.group({
        vehiculoNombre: ['', [Validators.required, Validators.maxLength(50)]],
        vehicleCapacidad: ['', [Validators.required, Validators.max(100), Validators.maxLength(10)]],
        vehicleTipo: ['', [Validators.required]]
      });
    } else {
      this.vehicleForm = this.formBuilder.group({
        vehiculoNombre: ['', []],
        vehicleCapacidad: ['', []],
        vehicleTipo: ['', []]
      });
    }
  }

  onSubmit() {
    this.backendService.postService(vehicle.apiSave,
    { }, this.vehicleModelInfo).then(
      (createVehicleResponse: GenericResponseModel) => {
        if (createVehicleResponse.respuesta === 'EXITO') {
          Swal.fire({
            title: 'Super tu servicio',
            text: '¡Que efectividad!',
            icon: 'success',
            confirmButtonText: 'Cool'
          }).then(
            () => {
              this.eventEmitter.emit('Cool');
            }
          )
        }
      }
    ).catch(
      (createVehicleError) => {
        console.error('Error guardando el vehiculo');
      }
    )
  }

  getVehicles() {
    this.backendService.getService(maestroValor.apiGet, { idTipoMaestroValor: 1
    }).then(
      (getTipoVehicleResult: any) => {
        this.tipoVehiculo = [...getTipoVehicleResult.listaRespuesta];
        console.log(this.tipoVehiculo)
      }
    ).catch(
      (getMasterError) => {
        console.error(getMasterError);
      }
    )
  }

  close() {
    this.ngbActiveModal.close();
  }

  get validatorForm() { return this.vehicleForm.controls; }
}
