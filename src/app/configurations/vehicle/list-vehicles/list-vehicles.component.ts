import { vehicle } from './../../../../constants/api-constants';
import { VehicleModel } from './../../../../models/entities/vehicle-model';
import { Component, OnInit } from '@angular/core';
import { BackendServiceService } from 'app/shared/backend-service/backend-service.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GenericResponseModel } from 'models/response/generic-response-model';
import { ComunicationModel } from 'models/utils/comunication-model';
import Swal from 'sweetalert2';
import { FormVehicleComponent } from '../form-vehicle/form-vehicle.component';

@Component({
  selector: 'app-list-vehicles',
  templateUrl: './list-vehicles.component.html',
  styleUrls: ['./list-vehicles.component.scss']
})
export class ListVehiclesComponent implements OnInit {

  vehicle: Array<VehicleModel> = new Array();

  columns = [
    { prop: 'id' },
    { prop: 'nombre' },
    { prop: 'capacidad' },
    { prop: 'tipoVehiculo' },
    { prop: 'options' }
  ];

  constructor(
    private backendService: BackendServiceService,
    private ngModal: NgbModal
  ) {
  }

  ngOnInit() {
    this.getvehicle();
  }

  getvehicle() {
    this.backendService.getService(vehicle.apiGet, { }).then(
      (getVehicleResult: GenericResponseModel) => {
        this.vehicle = [...getVehicleResult.listaRespuesta];
      }
    ).catch(
      (getVehicleError) => {
        console.error(getVehicleError);
      }
    )
  }

  viewVehicle(vehicleToView: any) {
    const modalOpened = this.ngModal.open(FormVehicleComponent, {
      backdrop: 'static',
      size: 'lg'
    });
    modalOpened.componentInstance.vehicleModelInfo = { ...vehicleToView };
    const comunicationModelInfo: ComunicationModel = new ComunicationModel();
    comunicationModelInfo.mode = 'view';
    modalOpened.componentInstance.comunicationModelInfo = comunicationModelInfo;
  }

  editVehicle(vehicleToView: any) {
    const modalOpened = this.ngModal.open(FormVehicleComponent, {
      backdrop: 'static',
      size: 'lg'
    });
    modalOpened.componentInstance.vehicleModelInfo = { ...vehicleToView };
    const comunicationModelInfo: ComunicationModel = new ComunicationModel();
    comunicationModelInfo.mode = 'edit';
    modalOpened.componentInstance.comunicationModelInfo = comunicationModelInfo;
    modalOpened.componentInstance.eventEmitter.subscribe(
      (response: any) => {
        if (response === 'Cool') {
          this.getvehicle();
          modalOpened.dismiss();
        }
      }
    )
  }

  createVehicle() {
    const modalOpened = this.ngModal.open(FormVehicleComponent, {
      backdrop: 'static',
      size: 'lg'
    });
    const comunicationModelInfo: ComunicationModel = new ComunicationModel();
    comunicationModelInfo.mode = 'create';
    modalOpened.componentInstance.comunicationModelInfo = comunicationModelInfo;
    modalOpened.componentInstance.eventEmitter.subscribe(
      (response: any) => {
        if (response === 'Cool') {
          this.getvehicle();
          modalOpened.dismiss();
        }
      }
    )
  }

  deleteVehicle(idToDelete: any) {
    this.backendService.deleteService(vehicle.apiDelete, {
      idVehiculoABorrar: idToDelete
    }).then(
      (deleteVehicleResult: GenericResponseModel) => {
        if (deleteVehicleResult.respuesta === 'EXITO') {
          Swal.fire({
            title: 'Super tu servicio',
            text: '¡Que efectividad!',
            icon: 'success',
            confirmButtonText: 'Cool'
          }).then(
            () => {
              this.getvehicle();
            }
          )
        }
      }
    ).catch(
      (deleteVehicleError) => {
        console.error(deleteVehicleError);
      }
    )
  }

}
