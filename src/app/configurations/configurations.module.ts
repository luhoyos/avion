import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListRatingsComponent } from './rating/list-ratings/list-ratings.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FormBenefitsComponent } from './benefits/form-benefits/form-benefits.component';
import { ListBenefitsComponent } from './benefits/list-benefits/list-benefits.component';
import { NgSelectModule } from '@ng-select/ng-select';

import { ConfigurationsRoutingModule } from './configurations-routing.module';
import { FormRatingsComponent } from './rating/form-ratings/form-ratings.component';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { ListVehiclesComponent } from './vehicle/list-vehicles/list-vehicles.component';
import { FormVehicleComponent } from './vehicle/form-vehicle/form-vehicle.component';

@NgModule({
  declarations: [ListRatingsComponent, FormRatingsComponent, ListVehiclesComponent, FormVehicleComponent,
    FormBenefitsComponent, ListBenefitsComponent],
  imports: [
    CommonModule,
    ConfigurationsRoutingModule,
    NgxDatatableModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModalModule,
    NgSelectModule
  ]
})
export class ConfigurationsModule { }
