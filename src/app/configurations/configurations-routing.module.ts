import { FormVehicleComponent } from './vehicle/form-vehicle/form-vehicle.component';
import { ListVehiclesComponent } from './vehicle/list-vehicles/list-vehicles.component';
import { FormBenefitsComponent } from './benefits/form-benefits/form-benefits.component';
import { FormRatingsComponent } from './rating/form-ratings/form-ratings.component';
import { ListRatingsComponent } from './rating/list-ratings/list-ratings.component';
import {ListBenefitsComponent} from './benefits/list-benefits/list-benefits.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'listar-tarifas',
        component: ListRatingsComponent
      },
      {
        path: 'crear-tarifas',
        component: FormRatingsComponent
      },
      {
        path: 'listar-beneficios',
        component: ListBenefitsComponent
      },
      {
        path: 'crear-beneficios',
        component: FormBenefitsComponent
      },
      {
        path: 'crear-vehiculos',
        component: FormVehicleComponent
      },
      {
        path: 'listar-vehiculos',
        component: ListVehiclesComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigurationsRoutingModule { }
