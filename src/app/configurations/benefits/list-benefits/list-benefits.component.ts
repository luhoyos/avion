import { BenefitsModel } from './../../../../models/entities/benefits-model';
import { Component, OnInit } from '@angular/core';
import { BackendServiceService } from 'app/shared/backend-service/backend-service.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBenefitsComponent } from './../form-benefits/form-benefits.component';
import Swal from 'sweetalert2';
import { benefits } from 'constants/api-constants';
import { ComunicationModel } from 'models/utils/comunication-model';
import { GenericResponseModel } from 'models/response/generic-response-model';

@Component({
  selector: 'app-list-benefits',
  templateUrl: './list-benefits.component.html'
})
export class ListBenefitsComponent implements OnInit {

  benefits: Array<BenefitsModel> = new Array();
  columns = [
    { prop: 'id' },
    { prop: 'nombre' },
    { prop: 'descripcion' },
    { prop: 'idTarifa' },
    { prop: 'options' }
  ];

  constructor(
    private backendService: BackendServiceService,
    private ngModal: NgbModal
    ) {
  }

  ngOnInit() {
    this.getBeneficts();
  }

  getBeneficts() {
    this.backendService.getService(benefits.apiGet, { }).then(
      (getBenefitsResult: any) => {
        this.benefits = [...getBenefitsResult.listaRespuesta];
      }
    ).catch(
      (getBenefitsgError) => {
        console.error(getBenefitsgError);
      }
    )
  }

  viewBenefit(benefitToView: any) {

    const modalOpened = this.ngModal.open(FormBenefitsComponent, {
      backdrop: 'static',
      size: 'lg'
    });
    modalOpened.componentInstance.benefitModelInfo = { ...benefitToView };
    const comunicationModelInfo: ComunicationModel = new ComunicationModel();
    comunicationModelInfo.mode = 'view';
    modalOpened.componentInstance.comunicationModelInfo = comunicationModelInfo;
  }

  editBenefit(benefitToView: any) {
    const modalOpened = this.ngModal.open(FormBenefitsComponent, {
      backdrop: 'static',
      size: 'lg'
    });
    modalOpened.componentInstance.benefitModelInfo = { ...benefitToView };
    const comunicationModelInfo: ComunicationModel = new ComunicationModel();
    comunicationModelInfo.mode = 'edit';
    modalOpened.componentInstance.comunicationModelInfo = comunicationModelInfo;
    modalOpened.componentInstance.eventEmitter.subscribe(
      (response) => {
        if (response === 'Cool') {
          this.getBeneficts();
          modalOpened.dismiss();
        }
      }
    )
  }

  createBenefit() {
    const modalOpened = this.ngModal.open(FormBenefitsComponent, {
      backdrop: 'static',
      size: 'lg'
    });
    const comunicationModelInfo: ComunicationModel = new ComunicationModel();
    comunicationModelInfo.mode = 'create';
    modalOpened.componentInstance.comunicationModelInfo = comunicationModelInfo;
    modalOpened.componentInstance.eventEmitter.subscribe(
      (response) => {
        if (response === 'Cool') {
          this.getBeneficts();
          modalOpened.dismiss();
        }
      }
    )
  }

  deleteBenefit(idToDelete: any) {
    this.backendService.deleteService(benefits.apiDelete, {
      idBeneficioABorrar: idToDelete
    }).then(
      (deleteRaitingResult: GenericResponseModel) => {
        if (deleteRaitingResult.respuesta === 'EXITO') {
          Swal.fire({
            title: 'Super tu servicio',
            text: '¡Que efectividad!',
            icon: 'success',
            confirmButtonText: 'Cool'
          }).then(
            () => {
              this.getBeneficts();
            }
          )
        }
      }
    ).catch(
      (deleteBenefitError) => {
        console.error(deleteBenefitError);
      }
    )
  }

}
