import { rating } from './../../../../constants/api-constants';
import { RatingModel } from './../../../../models/entities/rating-model';
import { benefits } from 'constants/api-constants';
import { BenefitsModel } from './../../../../models/entities/benefits-model';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BackendServiceService } from 'app/shared/backend-service/backend-service.service';
import { ComunicationModel } from 'models/utils/comunication-model';
import { GenericResponseModel } from 'models/response/generic-response-model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form-benefits',
  templateUrl: './form-benefits.component.html'
})
export class FormBenefitsComponent implements OnInit {

  benefitForm: FormGroup;
  tarifas: Array<RatingModel> = new Array();

  @Input() benefitModelInfo: BenefitsModel = new BenefitsModel();
  @Input() comunicationModelInfo: ComunicationModel = new ComunicationModel();
  @Output() eventEmitter: EventEmitter<any> =  new EventEmitter();

  constructor(
    private formBuilder: FormBuilder,
    private ngbActiveModal: NgbActiveModal,
    private backendService: BackendServiceService
  ) { }

  ngOnInit() {
    this.buildForm();
    this.getRatings();
  }

  buildForm() {
    if (!(this.comunicationModelInfo.mode === 'view')) {
      this.benefitForm = this.formBuilder.group({
        benefitNombre: ['', [Validators.required, Validators.maxLength(50)]],
        benefitDescripcion: ['', [Validators.required, Validators.maxLength(50)]],
        benefitTarifa: ['', [Validators.required]]
      });
    } else {
      this.benefitForm = this.formBuilder.group({
        benefitNombre: ['', []],
        benefitDescripcion: ['', []],
        benefitTarifa: ['', []]
      });
    }
  }

  onSubmit() {
    this.backendService.postService(benefits.apiSave,
    { }, this.benefitModelInfo).then(
      (createBenefitResponse: GenericResponseModel) => {
        if (createBenefitResponse.respuesta === 'EXITO') {
          Swal.fire({
            title: 'Super tu servicio',
            text: '¡Que efectividad!',
            icon: 'success',
            confirmButtonText: 'Cool'
          }).then(
            () => {
              this.eventEmitter.emit('Cool');
            }
          )
        }
      }
    ).catch(
      (createBenefitError) => {
        console.error('Error guardando el beneficio');
      }
    )
  }

  getRatings() {
    this.backendService.getService(rating.apiGet, {
    }).then(
      (getRaitingResult: any) => {
        this.tarifas = [...getRaitingResult.listaRespuesta];
      }
    ).catch(
      (getRaitingError) => {
        console.error(getRaitingError);
      }
    )
  }

  close() {
    this.ngbActiveModal.close();
  }

  get validatorForm() { return this.benefitForm.controls; }

}
