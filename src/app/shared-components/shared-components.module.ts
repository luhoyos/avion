import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedComponentsRoutingModule } from './shared-components-routing.module';
import { ListUsersComponent } from './list-users/list-users.component';

@NgModule({
  declarations: [ListUsersComponent],
  imports: [
    CommonModule,
    SharedComponentsRoutingModule,
    NgxDatatableModule
  ],
  exports: [
    ListUsersComponent
  ]
})
export class SharedComponentsModule { }
