import { FlyModel } from './../../../models/entities/fly-model';
import { avion } from './../../../constants/api-constants';
import { BackendServiceService } from './../../shared/backend-service/backend-service.service';
import { SecondaryControlsComponent } from './../secondary-controls/secondary-controls.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { GenericResponseModel } from 'models/response/generic-response-model';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ViewFlightComponent } from '../Visualization/view-vuelo.component';

@Component({
  selector: 'app-principal-controls',
  templateUrl: './principal-controls.component.html',
  styleUrls: ['./principal-controls.component.scss']
})
export class PrincipalControlsComponent implements OnInit {
  start = false;
  active = false;
  activeLand = false;
  viewReport = false;
  @ViewChild(ViewFlightComponent, null) viewFlightComponent: ViewFlightComponent;
  @ViewChild(SecondaryControlsComponent, null) secondaryControlsComponent: SecondaryControlsComponent;


  Models: FlyModel;
  idFlyOn: number;

  constructor(
    private backendServices: BackendServiceService
  ) { }

  ngOnInit() {
    this.takeOff();
    this.toLand();
    this.Home();
  }

  airAction(a: any) {
    switch (a) {
      case 'start':
        this.Comenzar();
        this.viewFlightComponent.startFly();
        this.start = true;
        break;
      case 'takeoff':
        this.secondaryControlsComponent.active = true;
        this.viewFlightComponent.takeOffUp();
        this.report('takeoff');
        this.active = false;
        break;
      case 'toland':
        this.viewFlightComponent.toLand();
        this.report('toland');
        break;
      case 'up':
        this.viewFlightComponent.up()
        this.report('up');
        break;
      case 'down':
        this.viewFlightComponent.down()
        this.report('down');
        break;
      case 'topos':
        this.viewFlightComponent.toAirplane(this.secondaryControlsComponent.altControl);
        this.report('topos');
        break;
      case 'ligth':
        this.viewFlightComponent.switchLigth();
        this.report('ligth');
        break;
      case 'eject':
        this.viewFlightComponent.peopleFalling.push(
          [this.viewFlightComponent.positionActualY, this.viewFlightComponent.positionActualX]
        );
        this.report('eject');
        break;
      default:
        break;
    }
  }

  takeOff() {
    if (!this.active) {
      if (this.viewFlightComponent.positionActualX >= this.viewFlightComponent.takeOffDistance) {
        this.active = true;
      } else {
        setTimeout(() => {
          this.takeOff();
        }, 2000);
      }
    }
  }

  toLand() {
    if (!this.activeLand) {
      if (this.viewFlightComponent.positionActualX >= this.viewFlightComponent.toLandDistance) {
        this.activeLand = true;
      } else {
        setTimeout(() => {
          this.toLand();
        }, 2000);
      }
    }
  }

  Home() {
    if (this.viewFlightComponent.positionActualX >= this.viewFlightComponent.cantX - 1) {
      this.viewReport = true;
    } else {
      setTimeout(() => {
        this.Home();
      }, 2000);
    }
  }

  async report(action: any) {
    this.Modelos(action)
    this.backendServices.postService(avion.apiSave, {}, this.Models).then(
      (getregistersResponse: GenericResponseModel) => {
        console.log(getregistersResponse.respuesta)
      }
    ).catch(
      (getReporteError) => {
        console.error(getReporteError);
      }
    )
  }

  Modelos(action: any) {
    this.Models = new FlyModel()
    this.Models.distanUser = this.viewFlightComponent.positionActualX;
    this.Models.altUser = this.viewFlightComponent.positionActualY;
  
    if (action === 'eject') {
      this.Models.personParacaida = 1;
    }
    if (action === 'fin') {
      this.Models.fechaRegistro = new Date();
    }


  }

  async Comenzar() {
    this.backendServices.getService(avion.apiGet, {}).then(
      (getIdFlyResponse: GenericResponseModel) => {
        this.idFlyOn = Number(getIdFlyResponse.variable);
        this.report('start');
        this.secondaryControlsComponent.idFlyOn=this.idFlyOn; 
      }
    ).catch(
      (getReporteError) => {
        console.error(getReporteError);
      }
    )
  }


  Controls() {
    this.secondaryControlsComponent.active = true;
    this.active = false;
    this.activeLand = false;
    this.report('fin');
  }
}






