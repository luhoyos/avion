import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-view-vuelo',
  templateUrl: './view-vuelo.component.html',
  styleUrls: ['./view-vuelo.component.scss']
})
export class ViewFlightComponent implements OnInit {

  @Output() eventEmitter: EventEmitter<any> = new EventEmitter();
  alturaMs = 50;
  largeMs = 40;
  speed = 1;
  takeOffDistance = 10;
  toLandDistance = 20;

  cantX: number;
  cantY: number;
  mapa = [];
  positionActualX: number;
  positionActualY: number;
  onLigth = false;
  flyActive = true;
  peopleFalling = [];
  upPos = 0;

  constructor() { this.dimension(); }

  ngOnInit() {
    this.fallPeople();
   }

  startFly() {
    if ( this.flyActive ) {
      this.fly()
    }
  }

  async fly() {
    if (this.positionActualX < this.cantX - 1) {
      this.mapa[this.positionActualY][this.positionActualX] = 1;
      this.positionActualX += this.speed;
      setTimeout(() => {
        this.startFly()
      }, 600);
    } else if (this.positionActualX === this.cantX - 1 && this.flyActive) {
      this.flyActive = false;
      Swal.fire({
        title: 'Detente!!!!!!',
        text: ' ¡¡¡¡Final del camino!',
        icon: 'success',
        confirmButtonText: 'Oks'
      }).then(
        () => {
          this.eventEmitter.emit();
        }
      )
    }
  }

  async dimension() {
    this.cantY = this.alturaMs;
    this.cantX = this.largeMs;

    const distanceArray = []
    for (let j = 0; j < this.cantX; j++) {
      distanceArray.push(0)
    }
    for (let i = 0; i < this.cantY; i++) {
      this.mapa.push([...distanceArray]);
    }
    this.positionFly();
  }

  async positionFly() {
    this.positionActualX = 0;
    this.positionActualY = this.mapa.length - 1;
  }

   calculate(int: number) {
    return Math.round(int / 1000);
  }

  up() {
    if ( this.positionActualY > 0) {
      this.positionActualY -= 1;
    } else {
      Swal.fire({
        title: 'Detente!!!!!!',
        text: ' ¡¡¡¡Final del camino!',
        icon: 'success',
        confirmButtonText: 'Oks'
      }).then(
        () => {
        }
      )
    }
  }

  down() {
    if ( this.positionActualY !== this.mapa.length - 1) {
      this.positionActualY += 1;
    } else {
      Swal.fire({
        title: 'Detente!!!!!!',
        text: ' ¡¡¡¡Final del camino!',
        icon: 'success',
        confirmButtonText: 'Oks'
      }).then(
        () => {
        }
      )
    }
  }

  paintAirPlane(x: number , y: number) {
    if (this.positionActualX === x && this.positionActualY === y) {
      return true;
    }
  }

  switchLigth() {
    this.onLigth = !this.onLigth;
  }

  isPeopleFallingHere(Alt: any, Dis: any) {
    let hasPeopleHere = false;
    this.peopleFalling.forEach(
      (personFall) => {
        if (personFall[0] === Alt && personFall[1] === Dis) {
          hasPeopleHere = true;
        }
      }
    )
    return hasPeopleHere;
  }

  fallPeople() {
    const indexOfPeopleToRemove = []
    for (let i = 0; i < this.peopleFalling.length; i++) {
      if (this.peopleFalling[i][0] === this.cantY - 1) {
        indexOfPeopleToRemove.push(i);
      } else {
        this.peopleFalling[i][0] += 1;
      }
    }

    indexOfPeopleToRemove.forEach(element => {
      this.peopleFalling.splice(element, 1);
    });

    setTimeout(() => {
      this.fallPeople();
    }, 1000);
  }

  toAirplane(toAirplaneAlt: any) {
    if (this.positionActualY === toAirplaneAlt) {
      this.readyPosition();
    } else if (this.positionActualY > toAirplaneAlt) {
      this.upAirplaneTo(toAirplaneAlt);
    } else {
      this.downAirplaneTo(toAirplaneAlt);
    }
  }

  upAirplaneTo(toAirplaneAlt: any) {
    if (this.positionActualY === toAirplaneAlt) {
      this.readyPosition();
    } else {
      this.positionActualY -= 1;
      setTimeout(() => {
        this.upAirplaneTo(toAirplaneAlt);
      }, 2000);
    }
  }

  downAirplaneTo(toAirplaneAlt: any) {
    if (this.positionActualY === toAirplaneAlt) {
      this.readyPosition();
    } else {
      this.positionActualY += 1;
      setTimeout(() => {
        this.downAirplaneTo(toAirplaneAlt);
      }, 2000);
    }
  }

  takeOffUp() {
    if (this.upPos < 3) {
      this.positionActualY -= 1;
      this.upPos++
      setTimeout(() => {
        this.takeOffUp();
      }, 1000);
    }
  }

  readyPosition() {
    Swal.fire({
      title: '¡Sigue Adelante!',
      text: '¡¡¡¡¡¡Estamos en posición Correcta!',
      icon: 'success',
      confirmButtonText: 'Cool'
    }).then(
      () => {
      }
    )
  }

  toLand() {
    if (this.positionActualY !== this.cantY - 1) {
      this.positionActualY += 1;
      setTimeout(() => {
        this.toLand()
      }, 2000);
    }
  }
}
