import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-secondary-controls',
  templateUrl: './secondary-controls.component.html',
  styleUrls: ['./secondary-controls.component.scss']
})
export class SecondaryControlsComponent implements OnInit {
  altControl: number;
  idFlyOn: number;

  @Output() eventEmitter: EventEmitter<any> = new EventEmitter();
  active = false;

  constructor(
  ) { }

  ngOnInit() {
  }

  action(reference: any) {
    this.eventEmitter.emit(reference);
  }

}
