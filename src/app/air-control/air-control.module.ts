import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AirControlRoutingModule } from './air-control-routing.module';
import { PrincipalControlsComponent } from './principal-controls/principal-controls.component';
import { SecondaryControlsComponent } from './secondary-controls/secondary-controls.component';
import { ViewFlightComponent } from './Visualization/view-vuelo.component';


@NgModule({
  declarations: [PrincipalControlsComponent, SecondaryControlsComponent, ViewFlightComponent],
  imports: [
    FormsModule,
    CommonModule,
    AirControlRoutingModule
  ]
})
export class AirControlModule { }
